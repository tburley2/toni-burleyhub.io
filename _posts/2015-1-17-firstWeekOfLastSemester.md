---
layout: post
title: It Came and It Went!
category: Lifestyle, Update
---
I just finished my very first week in my last semester as an undergrad!

Where has December gone?! I've been beating myself up for not sticking to this plan I made for the break. I feel like I was so unproductive. But I will have to say it's hard to get things done, that require concentration, with my little one running around.

But the break wasn't a complete fail. In the midst of all its chaos I was in the Pittsburgh Tribune Review [(click here to see the article)](http://triblive.com/news/allegheny/7400049-74/newsmaker-burley-development#axzz3P63w305U) and was able to attend a Code & Supply event and network!

Come to think of it, I just purchased not to long ago the [uncalendar](http://www.uncalendar.com/index.jsp) calendar for my planner. It's no wonder I never got anything accomplished when it comes to that strict schedule I made for the break. I am not good at strict schedules and things like that and not being able to express my creativity. Which is why I purchased those planner inserts in the first place!

Just going to continue to go with the flow and not allow myself to become too stressed that everything isn't going according to my plans. 

End of this Brain Dump!

