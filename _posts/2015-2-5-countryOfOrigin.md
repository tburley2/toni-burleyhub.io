---
layout: post
title: Paris is Why I Buy It!  
category: Lifestyle
draft: true
---

I never actually stopped to think about how the Country Of Origin (COO) actually effected my buying decisions and especially when it came to my makeup choices. It then dawned on me that brands like Dior, Chanel, Estee Lauder, Bobbi Brown, and Yves Saint Laurent were considered "high-end" brands and for a very good reason.  The majority of the lines COO are out of Europe. Both Italy and France carries the stereotype that everyone dresses like a Parisian and holds a high value to feminine beauty and aesthetics aside from the few negative ones.

But what about Aveda, Origins, and where most of my "Holy Grail" products come from-- MAC? After doing some research on their COO I realized that there is more than just the COO that effects my buying decision. MAC is from Canada whose stereotype of women is a little sketchy depending on where your POV is coming from. But I feel like you can't generalize them because it's a melting pot of ethnic groups. However, the women from Canada are perceived to be extremely feminine and more presentable when they walk out the house than Americans.

Their are a few "big name" companies that I now know (after the research) do not originate from countries in Europe.They are indeed large brands but they are not regularly purchased like the previous mentioned brands ( I don't know this could just be my own opinion).  I always walk past an Aveda  or an Origins and do just that -- walk past! The reason that walk past is because I think they are ridiculously expensive for no reason. They don't have a reputation like Yves Saint Laurent or Dior! In fact, Origins is from USA and if we had to follow stereotypes, I am not investing my hard earned coins into looking "ugly." LOL!!!!  All this time I have spent in and around makeup and it never dawned on me that my feelings about those lines all boiled down to the country of origin!

This explains why so I experienced so many women from all areas of the globe shelling out absurd amounts of money for Clarins products.

This idea of thinking lead to me to question why these countries had such a positive COO reputation and it was very interesting. Too far off subject for me to get into but affirmed that I was always meant to be a Parisian. (Just need to find that perfect skinny pill!) but I digress...

What I did grasp is that there is a big age gap in "beauty" and these brands are famous for "benefits" of their products. I'd never look to MAC for anything relating to skin care. If and when I do get into preserving my youth I'd be at Clarins in a heartbeat or Peter Thomas Roth for my budget!

I've always thought about branding myself and maybe coming out with a makeup line and this left me with a problem. My products COO would be USA and I would have to figure out how I'd get around the COO effect or make up for it. I guess that is to be continued....


I hope that this was good enough to cover the assignment and if not I completely understand.