---
layout: layout
title: "About"
---

#**Who is {{ site.author }} ?**
>I am a mother and full-time student obtaining my B.S in Information Technology at La Roche College. In my spare time I like to consume myself with anything "Web Development" and beauty related.

>Cosmetology, planning, and loose leaf teas provoke joy!

>As my mentor and [uncle](http://www.douglasmasonjr.com/) would say, "[I'm] a sponge right now...soaking up all the interweb knowledge."

#**The Reason I blog...**
{{ site.description }} 
I'll be journaling my triumphs and failures as I take on learning all that I can in my spare time.
